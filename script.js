let currentQuestionNumber = 1;
let lastAnsNumber;
let isGotAnswer = false;

const questionsAmount = 4;

const answersObj = {
	'1': 0,
	'2': 0,
	'3': 0
};

let personStatus;
let currentTaste;
let answersCombination = '';

function start() {
	startUfoAnimation();

	let typing = document.querySelector('.typing');
	typingText(typing);

	setStartButton();
	setAnswerButtons();
	setPopupButtons();
	setAnotherButton();

	setBuyButtons();
	setSmallBuyButtons();
	setLoadStickersButton();
	setShareButton();
	setFooterBuyButton();

	document.querySelector('#welcome_block').classList.add('active_dynamic_block');
}

function startUfoAnimation() {
	let indexMain = document.querySelector('.index_main');

	indexMain.classList.add('active_index_main');
}

function typingText(elem) {
	const text = elem.dataset.text;
	const arr = text.split('');
	const len = arr.length;

	let i = 0;

	setTimeout(function() {
		fillText();
	}, 1000);

	function fillText() {
		setTimeout(function() {
			if (i < len) {
				elem.textContent = elem.textContent + arr[i];
			} else {
				return;
			}

			i++;
			fillText();
		}, 100);
	}
}

function setPopupButtons() {
	let closePopupButton = document.querySelector('.close_popup_button');

	closePopupButton.addEventListener('click', function() {
		let popupsContainer = document.querySelector('.popups_container');

		popupsContainer.classList.remove('active_popups_container');
	});

	let edadealButton = document.querySelector('.edadeal_button');

	edadealButton.addEventListener('click', function() {
		ym(95552425,'reachGoal','keda');
		let popupsContainer = document.querySelector('.popups_container');

		popupsContainer.classList.add('active_popups_container');
	});
}

function showPopup() {
	let popupsContainer = document.querySelector('.popups_container');

	popupsContainer.classList.add('active_popups_container');
}

function setAnswerButtons() {
	let answerItems = document.querySelectorAll('.answer_item_button');

	answerItems.forEach(function(item) {
		item.addEventListener('click', function() {
			if (isGotAnswer) {
				return;
			}

			const ans = item.dataset.ans;
			const ansEvent = item.dataset.event;

			ym(95552425,'reachGoal', ansEvent);

			if (currentQuestionNumber == 1) {
				personStatus = ans;
			} else {
				answersCombination += ans;
				answersObj[ans]++;
			}

			if (currentQuestionNumber == 4) {
				lastAnsNumber = ans;
			}

			item.classList.add('active_answer_item_button');
			toNextQuestion();
		});
	});
}

function toNextQuestion() {
	isGotAnswer = true;

	setTimeout(function() {
		if (currentQuestionNumber == questionsAmount) {
			document.querySelector('#question_block').classList.remove('active_dynamic_block');
			document.querySelector('#load_block').classList.add('active_dynamic_block');
			checkAnswers();
		} else {
			hideQuestions();
			currentQuestionNumber++;

			let newQestion = document.querySelector('#question_' + currentQuestionNumber + '_' + personStatus);
			newQestion.classList.add('active_question_section_item');

			isGotAnswer = false;
		}
	}, 500);
}

function hideQuestions() {
	let questions = document.querySelectorAll('.question_section_item');

	questions.forEach(function(item) {
		item.classList.remove('active_question_section_item');
	})
}

function checkAnswers() {
	let tasteNumber;

	for (let key in answersObj) {
		if (answersObj[key] > 1) {
			tasteNumber = key;
		}
	}

	if (!tasteNumber) {
		switch (answersCombination) {
			case '123':
				tasteNumber = '1';
			break;
			case '132':
				tasteNumber = '3';
			break;
			case '213':
				tasteNumber = '1';
			break;
			case '231':
				tasteNumber = '2';
			break;
			case '312':
				tasteNumber = '2';
			break;
			case '321':
				tasteNumber = '3';
			break;
		}
	}

	setTaste(tasteNumber);

	setTimeout(function() {
		document.querySelector('#load_block').classList.remove('active_dynamic_block');
		document.querySelector('#result_block').classList.add('active_dynamic_block');
	}, 2500);
}

function setTaste(taste) {
	currentTaste = taste;
	let resultSections = document.querySelectorAll('.result_section');

	resultSections.forEach(function(item) {
		item.classList.remove('active_result_section');
	});

	let anotherItems = document.querySelectorAll('.another_item');

	anotherItems.forEach(function(item) {
		item.classList.remove('active_another_item');
	});

	let tasteEvent;

	switch(taste) {
		case '1':
			tasteEvent = 'psrtaca';
			document.querySelector('.cool_ocean_section').classList.add('active_result_section');
			document.querySelector('.dark_another_item').classList.add('active_another_item');
			document.querySelector('.ice_another_item').classList.add('active_another_item');
			break;
		case '2':
			tasteEvent = 'psrtdt';
			document.querySelector('.dark_temptation_section').classList.add('active_result_section');
			document.querySelector('.cool_another_item').classList.add('active_another_item');
			document.querySelector('.ice_another_item').classList.add('active_another_item');
			break;
		case '3':
			tasteEvent = 'psrtic';
			document.querySelector('.ice_chill_section').classList.add('active_result_section');
			document.querySelector('.cool_another_item').classList.add('active_another_item');
			document.querySelector('.dark_another_item').classList.add('active_another_item');
			break;
	}

	ym(95552425,'reachGoal', tasteEvent);

	switch(personStatus) {
		case '1':
			hideItems('son');
			hideItems('brother');
			showItems('boyfriend');
			setMusic(0);
			break;
		case '2':
			hideItems('brother');
			hideItems('boyfriend');
			showItems('son');
			setMusic(1);
			break;
		case '3':
			hideItems('son');
			hideItems('boyfriend');
			showItems('brother');
			setMusic(1);
			break;
	}

	startSwiper();
}

function hideItems(className) {
	let elems = document.querySelectorAll('.' + className);

	elems.forEach(function(item) {
		item.classList.add('hidden');
	});
}

function setBuyButtons() {
	let buttons = document.querySelectorAll('.buy_button');

	buttons.forEach(function(item) {
		item.addEventListener('click', function() {
			let dataEvent = item.dataset.event;

			ym(95552425,'reachGoal',dataEvent);
		});
	});
}

function setSmallBuyButtons() {
	let buttons = document.querySelectorAll('.buy_button_small');

	buttons.forEach(function(item) {
		item.addEventListener('click', function() {
			let dataTaste = item.dataset.taste;
			let event;

			switch (dataTaste) {
				case 'ice':
					if (currentTaste == 1) {
						event = 'kicnsaco';
					} else {
						event = 'kihnsdt'
					}
					break;
				case 'cool':
					if (currentTaste == 2) {
						event = 'kaconsdt';
					} else {
						event = 'kaconsic';
					}
					break;
				case 'dark':
					if (currentTaste == 1) {
						event = 'kdtnsaco';
					} else {
						event = 'kdtnsic';
					}
					break;
			}

			ym(95552425,'reachGoal',event);
		});
	});
}

function setLoadStickersButton() {
	const loadStickersButton = document.querySelector('.load_stickers_button');

	loadStickersButton.addEventListener('click', function() {
		let event;

		switch (currentTaste) {
			case 1:
				event = 'ssnsaco';
				break;
			case 2:
				event = 'ssnsdt';
				break;
			case 3:
				event = 'ssnsic';
				break;
		}

		ym(95552425,'reachGoal',event);
	});
}

function showItems(className) {
	let elems = document.querySelectorAll('.' + className);

	elems.forEach(function(item) {
		item.classList.remove('hidden');
	});
}

function setStartButton() {
	ym(95552425,'reachGoal','kvavp');
	let startButton = document.querySelector('.start_button');

	if (!startButton) {
		return
	}

	startButton.addEventListener('click', function() {
		document.querySelector('#welcome_block').classList.remove('active_dynamic_block');
		document.querySelector('#question_block').classList.add('active_dynamic_block');
	});
}

function findParentByClassName(parentClassName, elem) {
    while(elem !== document.body) {

        if(elem.classList.contains(parentClassName)) {
            return elem;
        }

        elem = elem.parentNode;
    }
}

function setAnotherButton() {
	let anotherAttemptButton = document.querySelector('.another_attempt_button');

	if (!anotherAttemptButton) {
		return
	}

	anotherAttemptButton.addEventListener('click', function() {
		let resultSections = document.querySelectorAll('.result_section');

		resultSections.forEach(function(item) {
			item.classList.remove('active_result_section');
		});

		let anotherItems = document.querySelectorAll('.another_item');

		anotherItems.forEach(function(item) {
			item.classList.remove('active_another_item');
		});

		let answerItems = document.querySelectorAll('.answer_item_button');

		answerItems.forEach(function(item) {
			item.classList.remove('active_answer_item_button');
		});

		currentQuestionNumber = 1;
		answersCombination = '';

		hideQuestions();

		document.querySelector('#question_1').classList.add('active_question_section_item');

		answersObj['1'] = 0;
		answersObj['2'] = 0;
		answersObj['3'] = 0;

		isGotAnswer = false;

		document.querySelector('#welcome_block').classList.add('active_dynamic_block');
		document.querySelector('#result_block').classList.remove('active_dynamic_block');

		let event;
		switch (currentTaste) {
			case 1:
				event = 'pernsaco';
				break;
			case 2:
				event = 'pernsdt';
				break;
			case 3:
				event = 'pernsic';
				break;
		}

		ym(95552425,'reachGoal','kproi');
		ym(95552425,'reachGoal',event);
	});
}

function setShareButton() {
	let shareButton = document.querySelector('.share_button');

	shareButton.addEventListener('click', function() {
		let event;

		switch (currentTaste) {
			case 1:
				event = 'pnaco';
				break;
			case 2:
				event = 'pnsdtemp';
				break;
			case 3:
				event = 'pnsic';
				break;
		}

		ym(95552425,'reachGoal','kpod');
		ym(95552425,'reachGoal',event);
	});
}

function setFooterBuyButton() {
	const footerBuyButton = document.querySelector('.footer_buy_button');

	footerBuyButton.addEventListener('click', function() {
		let event;

		switch (currentTaste) {
			case 1:
				event = 'knyanaco';
				break;
			case 2:
				event = 'knyamnsdt';
				break;
			case 3:
				event = 'knyamnsic';
				break;
		}

		ym(95552425,'reachGoal',event);
	});
}

function startSwiper() {
	let oldSwiper = document.querySelector('.filmes_swiper');

	if (oldSwiper) {
		oldSwiper.remove();
	}

	let filmesSwiper = document.createElement('div');
	filmesSwiper.classList.add('filmes_swiper');

	let swiperWrapper = document.createElement('div');
	swiperWrapper.classList.add('swiper-wrapper');

	fillSwiper(swiperWrapper);
	filmesSwiper.append(swiperWrapper);

	let filmesYandexSide = document.querySelector('.filmes_yandex_side');
	filmesYandexSide.append(filmesSwiper);
	
	const swiper = new Swiper('.filmes_swiper', {
		direction: 'horizontal',
		loop: true,
		slidesPerView: 'auto',
		centeredSlides: true,
	});
}

function fillSwiper(wrapper) {
	let slides;

	switch(personStatus) {
		case '1':
			slides = document.querySelectorAll('.slide-boyfriend');
			break;
		case '2':
			slides = document.querySelectorAll('.slide-son');
			break;
		case '3':
			slides = document.querySelectorAll('.slide-brother');
			break;
	}

	slides.forEach(function(item) {
		let clone = item.cloneNode(true);

		clone.addEventListener('click', function() {
			let event;

			switch (currentTaste) {
				case 1:
					event = 'vfnsaco';
					break;
				case 2:
					event = 'vfnsdt';
					break;
				case 3:
					event = 'vansic';
					break;
			}

			ym(95552425,'reachGoal',event);
		});

		wrapper.append(clone);
	});
}

function setMusic(number) {
	let musics = [
		`<iframe frameborder="0" style="border:none;width:100%;height:100%;" width="100%" height="100%" src="https://music.yandex.ru/iframe/playlist/axeyamusic/1001">
			Слушайте <a href='https://music.yandex.ru/users/axeyamusic/playlists/1001'>Вечер на двоих</a> — <a href='https://music.yandex.ru/users/axeyamusic'>axeyamusic</a> на Яндекс Музыке</iframe>`,
		`<iframe frameborder="0" style="border:none;width:100%;height:100%;" width="100%" height="100%" src="https://music.yandex.ru/iframe/playlist/axeyamusic/1000">
			Слушайте <a href='https://music.yandex.ru/users/axeyamusic/playlists/1000'>В кругу семьи</a> — <a href='https://music.yandex.ru/users/axeyamusic'>axeyamusic</a> на Яндекс Музыке</iframe>`
	];

	let musicYandexSide = document.querySelector('.music_yandex_side');
	musicYandexSide.innerHTML = '';

	if (musics[number]) {
		musicYandexSide.innerHTML = musics[number];
	} else {
		musicYandexSide.innerHTML = musics[1];
	}
}

setHeight();

function setHeight() {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
}

window.addEventListener('load', function() {
	start();
});